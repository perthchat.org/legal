## Privacy Policy - The Perthchat.org Matrix Service

This chapter applies to the Matrix service provided at https://perthchat.org

When you read 'the Perthchat homeserver', 'Perthchat' or 'the Service' below, it refers to the services made available at perthchat.org which store your account and personal conversation history, provide services such as bots and bridges, and communicate via the open Matrix decentralised communication protocol with the public Matrix Network.

The public Matrix Network is a decentralized and openly federated communication network. This means that user messages are replicated on each participant's server and messages posted to a room are visible to all participants including in some cases any new joiners. This is further explained below.

Where you read Perthchat.org, Perthchat, PERTHCHAT.ORG INC or we, our, or us below, all refer to the same company.

---

### 1. Right to Erasure

You can request that we forget your copy of messages and files by instructing us to deactivate your account (using a Matrix client such as https://client.perthchat.org) and selecting the option instructing us to forget your messages. What happens next depends on who else had access to the messages and files you had shared.

Any messages or files that were only accessible by your account will be deleted from our servers within 30 days.

Where you shared messages or files with another registered Matrix user, that user will still have access to their copy of those messages or files. Apart from state events (see below), these messages and files will not be shared with any unregistered or new users who view the room after we have processed your request to be forgotten.

State events are processed differently to non-state events. State events are used by the Service to record, amongst other things, your membership in a room, the configuration of room settings, your changing of another user's power level and your banning a user from a room. Were we to erase these state events from a room entirely, it would be very damaging to other users' experience of the room, causing banned users to become unbanned, revoking legitimate administrator privileges, etc. We therefore share state events sent by your account with all non-essential data removed ('redacted'), even after we have processed your request to be forgotten. This means that your username will continue to be publicly associated with rooms in which you have participated, even after we have processed your request to be forgotten. If this is not acceptable to you, please do not use the Service.

---

### 2. What Information Do You Collect About Me and Why?

The information we collect is purely for the purpose of providing your communication service via Matrix. We do **not** profile users or their data on the Service.

Be aware that while we do not profile users on the Service, third party Matrix clients may gather usage data. This includes the Matrix client Element provided at https://client.perthchat.org which optionally gathers opt-in anonymised usage data under New Vector Ltd.'s privacy notice at https://element.io/privacy.


#### 2.1 Information you provide to us:

We collect information about you when you input it into the Service or otherwise provide it directly to us.

Account and Profile Information

We collect information about you when you register for an account. This information is kept to a minimum on purpose, and is restricted to:

    - Username
    - Password (hashed)
    - Display Name (if you choose to provide one)
    - Your email address (if you choose to provide it)
    - Your verified telephone number (if you choose to provide it)

Your username and password is used to authenticate your access to the Service and to uniquely identify you within the Service.

Your password is stored until you change it or your account is deactivated. Your username is stored indefinitely to avoid account recycling.

Your email address and/or telephone number, if you choose to provide them, are used so that other users can look up your Matrix ID from these identifiers. These are stored and processed by the chosen 3rd party identity service such as vector.im. Their privacy policy apply.

We will also use your email address to let you reset your password if you forget it, and to send you notifications about missed messages from users trying to contact you on Matrix if you enable the option. We may also send you infrequent urgent messages about platform updates.

Content you provide through using the Service

We store and distribute the messages and files you share using the Service (and across the wider Matrix ecosystem via federation) as described by the Matrix protocol and according to the access rules configured within the system. Storing and sharing this content is the reason the Service exists.

This content includes any information about yourself that you choose to share.


#### 2.2 Information we collect automatically as you use the Service:

Device and Connection Information

Each device you use to access the Service is allocated a (user-configurable) identifier. When you access the Service, we record the device identifier, the IP address it used to connect, user agent, and the time at which it last connected to the service.

This information is gathered to help you to manage your devices - you can view and manage the list of devices by connecting to the Service with a Matrix client such as https://client.perthchat.org

Currently, we log the IP addresses of everyone who accesses the Service. This data is used in order to mitigate abuse, debug operational issues, and monitor traffic patterns. Our logs are kept for not longer than 7 days.

---

### 3. What Information is Shared With Third Parties and Why?

#### 3.1 Sharing Data with Connected Services

We may share your information when working with our suppliers in order to provide the Service.

In addition, Perthchat is a decentralised and open service. This means that, to support communication between users on different homeservers or different messaging platforms, your username, display name and messages and files are sometimes shared with other services that are connected with the Perthchat. These other services may be outside of the EU.

**Federation**

Matrix homeservers share user data with the wider ecosystem over federation.

When you send messages or files in a room, a copy of the data is sent to all participants in the room, including (depending on room settings) participants who join the room in future. If these participants are on remote homeservers, your username, display name, messages and files may be replicated across each participating homeserver.

We will forget your copy of your data upon your request. We will also forward your request to be forgotten onto federated homeservers. However - these homeservers are outside our span of control, so we cannot guarantee they will forget your data.

Federated homeservers can be located anywhere in the world, and are subject to local laws and regulations.

Access control settings are shared between homeservers, as well as any requests to remove messages by "redactions", or remove personal data under GDPR Article 17 Right to Erasure (Right to be Forgotten). Federated homeservers and Matrix clients which respect the Matrix protocol are expected to honour these controls and redaction/erasure requests, but other federated homeservers are outside of the span of control of PERTHCHAT.ORG INC, and we cannot guarantee how this data will be processed. Federated homeservers can also be located in any territory, and will be subject to the local regulations of that territory.

**Bridging**

Some Matrix rooms are bridged to third-party services, such as IRC networks, Twitter or email. When a room has been bridged, your username, display name, messages and file transfers may be duplicated on the bridged service where supported.

It may not be technically possible to support your management of your data once it has been copied onto a bridged service.

Bridged services can be located anywhere in the world, and are subject to local laws and regulations.

Access control settings, requests to remove messages by "redactions" or remove personal data under GDPR Article 17 Right to Erasure (Right to be Forgotten) are shared to bridging services, which are expected to honour them to the best of their ability. Be aware that not all bridged networks or bridges support the necessary technical capabilities to limit, remove or erase messages. If this is not acceptable to you, please do not use bridged rooms.


#### 3.2 Sharing Data in Compliance with Enforcement Requests and Applicable Laws; Enforcement of Our Rights

In exceptional circumstances, we may share information about you with a third party if we believe that sharing is reasonably necessary to

    (a) comply with any applicable law, regulation, legal process or governmental request,
    (b) protect the security or integrity of our products and services (e.g. for a security audit),
    (c) protect PERTHCHAT.ORG INC and our users from harm or illegal activities, or
    (d) respond to an emergency which we believe in good faith requires us to disclose information to assist in preventing the serious bodily harm of any person.

---

### 4. How Do You Handle Passwords?

We never store password data in plain text; instead they are stored hashed (with at least 4096 rounds of bcrypt, including both a salt and a server-side pepper secret). Passwords sent to the server are encrypted using TLS.

It is your sole responsibility to keep your user name, password and other sensitive information confidential. Actions taken using your credentials shall be deemed to be actions taken by you, with all consequences including service termination, civil and criminal penalties.

If you become aware of any unauthorized use of your account or any other breach of security, you must notify PERTHCHAT.ORG INC immediately by sending us an email. Suspicious devices can be deleted using the User Settings management tools in a Matrix client such as https://client.perthchat.org, and users should manage good password hygiene (e.g. using a password manager) and change their password if they believe their account is compromised.

If you forget your password (and you have registered an email address) you can use the password reset facility to reset it.

You can manage your account by using a Matrix client such as https://client.perthchat.org.

We will never change a password for you.

---

### 5. How Can I Access or Correct My Information?

You can access all your personally identifiable information that we collect by using any compatible Matrix client (such as https://client.perthchat.org) and managing your User Settings. You can get a copy of all your data.

---

### 6. Who Can See My Messages and Files?

In unencrypted and encrypted rooms, users connecting to Perthchat (directly or over federation) will be able to see messages and files according to the access permissions configuration of the relevant room. This data is stored in the format it was received on our servers, and can be viewed by Perthchat engineers (employees and contractors) under the conditions outlined below.

In encrypted rooms, the data is stored in our databases but the encryption keys are stored only on your devices or by yourself. Users can optionally backup an encrypted copy of their keys on the Service to aid recovery if they lose all their keys and devices. This key backup is encrypted by a recovery key that only the user has access to. This means that nobody, even Perthchat engineers (employees and contractors) can see your message content in our database, and if you lose access to your encryption keys you lose access to your messages forever.

We use HTTPS to transfer all data. End-to-end encrypted messaging data is stored encrypted using AES-256, using message keys generated using the Olm and Megolm cryptographic ratchets.

---

### 7. What Are the Guidelines Perthchat Follows When Accessing My Data?

We restrict who at PERTHCHAT.ORG INC (employees and contractors) can access user data to roles which require access in order to maintain the health of the Service.

We never share what we see with other users or the general public.

---

### 8. Who Else Has Access to My Data?

This service is hosted at PCHQ data centre.

We use secure private keys when accessing servers via SSH.

We log application data (username, user IP and user agent). We keep logs for no longer than 28 days.

---

## Privacy Policy - The 'Perthchat' Android Application

This chapter applies to the Perthchat application that's found on the Google Play Store.

By downloading and using the Perthchat application you are agreeing to all sections of this privacy policy.

The Perthchat application requests the following permissions:

    - 'CAMERA' and 'RECORD_AUDIO' permissions, these are needed to allow the user to collect photos/video and audio recordings.
    - 'READ_CONTACTS' permission, this information is shared with our [ma1sd identity server](https://github.com/ma1uta/ma1sd) and can help connect you to other users you know who have set a 3PID with their account (for example an email or phone number).
    - 'ACCESS_COARSE_LOCATION' and 'ACCESS_FINE_LOCATION' permissions, this information us used to allow you to share your location with other Matrix users.

Information collected by the Perthchat application is not used for advertising and is not shared with any 3rd parties.
