## Terms of Service

By creating a [perthchat.org](https://perthchat.org) account are agreeing that you are at least 18 years old and that you’ll be responsible for the account you’ve created. You also agree to never participate in the following activities while using perthchat:

    1. Advertising.
    2. Spamming or fraud.
    3. Evading the bans or restrictions of other homeservers.
    4. Racism or intolerance.
    5. Brigading or trolling attacks on other users, rooms or homeservers.
    6. Illegal activity.

You are also responsible for any room that you create on Matrix with your perthchat.org account, listing any room on our public room list will require that the room is at least moderated according to the rules mentioned above.

See [#codeofconduct:perthchat.org](https://matrix.to/#/#codeofconduct:perthchat.org) if you see a rule being broken or wish to dispute a moderation decision.
