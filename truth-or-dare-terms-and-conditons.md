# TERMS AND CONDITIONS FOR TRUTH-OR-DARE YOUTUBE CHALLENGE

Effective Date: July 9, 2023

These terms and conditions ("Terms") govern the monthly truth-or-dare challenge ("Challenge") conducted by Michael Collins ("Host") for the highest contributor on the Patreon fundraiser ("Participant"). Please read these Terms carefully. By participating in the Challenge, you agree to abide by these Terms.

1. ELIGIBILITY

1.1 The Participant must be the highest donor on the Host's Patreon page for the given month.

1.2 Participants must be at least 18 years of age, in accordance with Australian laws.

1.3 Employees, contractors, directors, and officers of the Host's company, and immediate family members of any such persons are not eligible to participate.

2. CHALLENGE RULES

2.1 The Challenge will involve the Participant and the Host engaging in a truth-or-dare game which will be recorded and potentially uploaded to YouTube.

2.2 The Participant must respect the following limitations on dare challenges:

a) No dares shall involve nudity or explicit content.

b) No dares shall involve any sexual content or innuendos.

c) No dares shall require more than 3 hours to complete.

d) No dares shall involve activities that carry a risk of permanent physical injury.

e) No dares shall involve any costs, unless the Participant agrees to cover such costs.

3. DISQUALIFICATION

The Host reserves the right to disqualify a Participant if they fail to comply with the rules outlined in Section 2, or for any other reason deemed necessary in the sole discretion of the Host.

4. LIABILITY RELEASE

4.1 By participating in the Challenge, the Participant releases and holds the Host harmless from any and all liability for any injuries, loss, or damage of any kind arising from or in connection with the Challenge.

4.2 The Participant agrees to indemnify, defend, and hold harmless the Host from any and all claims arising out of violation of these Terms.

5. USE OF PERSONAL DATA

By participating in the Challenge, the Participant consents to the Host's use and disclosure of Participant's personal information for the purposes of administering the Challenge and any related publicity, in accordance with the Privacy Act 1988 (Cth) of Australia.

6. GOVERNING LAW

These Terms are governed by the laws of Australia. Any disputes arising out of these Terms will be subject to the exclusive jurisdiction of the courts of Australia.

7. ACCEPTANCE OF TERMS

Participation in the Challenge constitutes acceptance of these Terms. If you do not agree to these Terms, please refrain from participating in the Challenge.

The Host reserves the right to change, modify, or update these Terms at any time and for any reason, without prior notice. Continued participation in the Challenge following any changes, modifications, or updates constitutes acceptance of those changes.

8. CONTACT

For any queries related to these Terms or the Challenge, please contact the Host at the provided contact information on the Patreon page.
